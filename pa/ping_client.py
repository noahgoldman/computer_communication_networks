import socket
import time
import sys
import string
import argparse

PING_STR = 'PING'

# Initialize the socket and set its timeout to 1 second
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.settimeout(1)

# Calculate the total time in milliseconds, from a floating-point seconds
# value
def seconds_to_millis(seconds):
    return seconds * (10**3)

# Remove non-printable characters from a string
def remove_nonprintable(st):
    return filter(lambda x: x in string.printable, st)

# Send a ping message with the appropriate format to the address, and
# return the timestamp sent with the ping.
def send_ping(addr, seq):
    # Create the message to be sent
    msg = "{} {} {}".format(PING_STR, str(seq), str(time.time()))

    # Send the message over UDP. Raise an exception if not all bytes were sent
    sent = sock.sendto(msg, addr)
    if sent != len(msg):
        raise Exception("Failed to send {} to {}".format(msg, addr))

# Wait and receive a PING message. Return a timestamp, or None if the message wasn't
# received in 1 second
def recv_ping():
    # Try and receive data, but throw an exception if there's an error or timeout
    try:
        data = sock.recv(4096)
    except socket.error:
        return None

    # Parse out the words in the message, verify that it start with "PING", then
    # return the timestamp
    words = data.split()
    assert words[0] == PING_STR
    return float(remove_nonprintable(words[2]))

# Send and receive a PING message from the server, then return RTT information
# if the segment is sent back
def ping(addr, seq):
    # Send and receive the timestamp
    send_ping(addr, seq)
    start_ts = recv_ping()

    output = ' '.join([PING_STR, socket.gethostbyname(addr[0]), str(seq)])

    # If the response wasn't received, return a message saying the segment was lost
    if start_ts is None:
        return output + ' LOST'

    # Compute the total RTT time, and return it
    total = seconds_to_millis(time.time() - start_ts)
    return '{} {:.3f}'.format(output, total)

# Send a receive a PING message from the server, retrying 10 times if the packet
# is lost. Return true if delivered, else return false
def ping_stop_and_wait(addr, seq):
    for i in range(10):
        # Return true when the ping succeeds
        if try_ping(addr, seq):
            return True

    return False

# Return true or false if a ping was successfully transmitted or not
def try_ping(addr, seq):
    try:
        send_ping(addr, seq)
    except:
        # If there was an exception, return false
        return False
    return recv_ping() != None # Return false if the receive timed out

# Main procedure for part A
def part_a(addr):
    iterations = 5

    # Send packets and print their RTT, or a 'LOST' message if necessary
    for i in range(iterations):
        print(ping(addr, i))

        # Don't wait an entire second after the last packet is sent.
        if i != iterations - 1:
            time.sleep(1)

# Main procedure for part b (the reliable protocol)
def part_b(addr):
    iterations = 5
    start = time.time() # Get the time we start sending packets

    for i in range(iterations):
        # If the sending of any packet fails (after 10 retriest), print a message
        # and return
        if not ping_stop_and_wait(addr, i):
            print("Failed to PING {}:{} (sequence number: {})".format(
                addr[0], str(addr[1]), i))
            return None

    delta = time.time() - start

    # Print the total time it took to deliver all the packets
    print("All {} PINGs completed.  Total Delivery Time={}s".format(
        iterations, delta))

    return delta

# Initialize command line argument parsing.  Call the program with "-h" to see help text
def init_args():
    parser = argparse.ArgumentParser(description='CCN Programming Assignment 1 by Noah Goldman')
    parser.add_argument('host', help="The hostname to ping", type=str)
    parser.add_argument('port', help="The port on which the server is running", type=int)
    parser.add_argument('part', help="Specify either 'a' or 'b' corresponding to the part to run", type=str)

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = init_args()

    # Create the address as a typle that the socket module can understand
    addr = (args.host, args.port)

    # Conditional to handle the different "parts" of the homework
    if args.part == 'a':
        part_a(addr)
    elif args.part == 'b':
        part_b(addr)
    else:
        print("You must specify either 'a' or 'b'")
