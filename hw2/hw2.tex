\documentclass{article}

\usepackage{csquotes}

\begin{document}

\title{Homework 2}
\author{Noah Goldman}
\maketitle

\section*{Problem 1}

\subsection*{Part A}

In general, persistent HTTP with pipelining should be more efficient than non-persistent HTTP with parallel connections.  If we define efficiency as transferring the least amount of data possible, then persistent HTTP with pipelining has significant advantage.  Both methods have the ability to make multiple requests to the server at once without waiting for a response on any specific one, but non-persistent HTTP has to open a new TCP connection for each request made.  This results in non-persistent HTTP having to deal with much more round-trip delay time from the TCP two-way handshake, which can potentially be repeated many times.  In contrast, persistent HTTP with pipelining only needs to establish a single TCP connection, and then can send many requests over it without more overhead from the transport layer.  This difference results in persistent HTTP with pipelining being more efficient than non-persistent HTTP with parallel connections.

While the comparison above was made primarialy in the context of efficiency, this also translates to a similar comparison in terms of speed.  Since persistent HTTP with pipelining spends less time waiting to establish TCP connections, it can be faster than parallel connections with no persistence.  However, the nature of persistent HTTP with pipelining is that the responses from the server must be sent back in the order that they were made, in sequence.  If a client requests a large file first, it could result in the transfer of many smaller files being blocked due to congestion on the socket. Non-persistent HTTP with parallelization avoids this problem for multi-threaded servers, as they can send back many files at once over the multiple connections.  While this does show that non-persistent HTTP with parallel connections could be faster in some scenarios, it does not change the fact that persistent HTTP with pipelining is more efficient.

\subsection*{Part B}

One of the primary differences between HTTP and FTP is that HTTP uses a single TCP connection for all commands and data that are sent between the client and server.  In contrast, FTP utilizes two TCP connections: one for sending control information like commands and acknowledgements, and another for transferring larger amounts of data like files or directory listings. When an FTP connection is initially established, the control connection is opened and used for authentication and commands.  Whenever a large data transfer is required, the client and server open the data connection and send the information using it.  On the other hand, HTTP simply sends commands and data over a single connection.

Another difference between HTTP and FTP is how they handle state between the client and server.  Each FTP server needs to keep track of a good amount of information about its client, like their user credentials, the current directory, or the user permissions.  The client can make multiple commands to the server, while knowing that the state persists throughout the session.  On the other hand, HTTP has no built-in concept of state.  Each request is assumed to be completely independent from another, and dealing with situations like user authentication requires the client to repeatedly send its information to the server every time it makes a request.  Overall, HTTP has no concept of state, while FTP uses it significantly.

\subsection*{Part C}

When a client is connecting to a remote host, it is important that that the server is running on a port that is known by the client. Since there's no efficient way for a client to scan ports to determine on which the server is running, its important to have a well-known port that that any server for a given service is running on.  With an established standard, a client can simply connect to any host at that port and assume it points to the correct server.  A client might know the IP address of the server it wants to connect to, but without knowing the port it is running on, cant actually establish a connection.

Since clients are, by nature, the ones to initiate contact with the server, they can specify any local port they'd like in their opening response for the server to connect back to.  It is not necessary for the client port to be well-established, as the server is able to derive it from the initial request.  Therefore, client ports can be chosen almost at random and can be completely ephemeral.  This can allow for a good allocation of resources on the client, as ports can be given out from a open pool by the operating system, and then returned back once their lifetime is complete.

\subsection*{Part D}

\begin{itemize}
    \item \textbf{HTTP:} 80
    \item \textbf{FTP Data:} 20
    \item \textbf{IMAP:} 143
\end{itemize}

\section*{Problem 2}

\subsection*{Part I}

As seen from the Wireshark capture file "problem\_2\_a.pcap", the web browser fetches the files:

\begin{itemize}
    \item / (the root of "http://www.cs.rpi.edu/")
    \item /templates/cs.css
    \item /images/logo\_rpics.gif
    \item /images/amoseaton.jpg
    \item /images/blue\_rule.gif
    \item /images/rpi\_bug.gif
    \item /images/bullet\_square
    \item /images/transparent
    \item /images/search\_arrow.gif
\end{itemize}

These files were fetched using many different HTTP GET requests, with the requests themselves being sent in the above order.  While it might make intuitive sense for all of these files to be fetched using a single request, the architecture of HTTP is such that the client doesn't know all of the resources it will need before it makes the original request.  The process of loading the web page begins with sending a GET request to the specified address (in our case "/").  The subsequent response from the server will contain the HTML body of the page, which includes directives specifying other files that are needed.  Only after processing this response is the browser able to make HTTP requests for the rest of the required files.  It also seems as if making a single request for all these resources specified by the original page would be more efficient, but this approach could present problems, as pages often require files from multiple different hosts (or servers).

\subsection*{Part II}

Although it isn't clearly a part of the HTTP specification, the attached capture file "problem\_2\_a.pcap" shows the usage of parallel and non-persistent HTTP connections, as well as parallel and persistent connections.  Following the TCP stream associated with the GET request for "/" (packet number 9) shows that the connection is reused multiple times to retrieve more resources.  Each request is made by waiting for the full response to return before starting the next one, showing that pipelining is not in use.  However, at the same time, multiple other TCP connections are being used in parallel as well (those initialized with packet numbers 1-6).  While this isn't clearly a standardized way of using HTTP, it may be a working optimization that Google Chromium utilizes.

\subsection*{Part III}

As can be seen in the attached capture file "problem\_2\_a.pcap", the client sends 10 HTTP messages to the server, all of which are GET requests.  To respond to each of these requests with the appropriate data, the server also sends 10 HTTP messages back to the client.

In the case of messages sent from the client to the server, relatively little data is transferred. Therefore, each HTTP message can fit in a single packet, and 10 total packets are used to send the HTTP requests from the client to the server.  In contrast, the server's responses to the client often contained a significant amount of data in each request.  Sending images or HTML was done over TCP, often using multiple packets.  The total number of packets used to send these messages was 19.

\subsection*{Part IV}

The primary difference that can be seen between experiments "a" and "b", is that none of the files listen in Part I besides the HTML of the page (the path "/") are actually sent from the server to the client.  The same GET requests are made for all the files listed in Part I, but instead of responding with the raw data sent over TCP and an HTTP status code of 200, the server just sends an HTTP response with the code 304.  This status code signifies that the version of the file cached by the browser is the same as the version currently on the server.  As a result, the server reduces bandwith consumption by not sending the data for the file over the network again.

\section*{Problem 3}

\subsection*{Part A}

As found in the capture file, the IP address of the laptop was 192.168.1.104, and the address of the RCS FTP server is 128.113.26.86. Since the IP address of the laptop was in the 192.168.x.x range and not the 10.x.x.x range normally found on the RPI LAN, the laptop was connected at the professor's home.

\subsection*{Part B}

Judging from the capture file, the professor first authenticated as "kark" with the password "pass1234".  He then retrieved the file "hello.c" from the remote server, and in the process the FTP client changed to ASCII mode.  Finally, he closed the connection.

\subsection*{Part C}

Over the course of the FTP session, two TCP connections were used.  The FTP protocol uses a control connection (a single TCP connection) to send information like user identification, commands to the server, and server acknowledgements of these commands. It also uses a separate data connection (a different TCP connection) to transfer files and similar types of data, like directory listings.

For the session described by the capture file, the control connection used port 1226 on the client side, and port 21 on the server side.  The data connection that was created to transfer the file "hello.c" used port 5001 on the client side, and 20 on the server side.

\subsection*{Part D}

For the single file transfer of "hello.c", the TCP segment with packet number 32 was used to transfer the data.  This sequence was started with the TCP ACK from the server to the laptop with packet number 29, and the transfer was acknowledged by the client with packet number 33.

\section*{Problem 4}

\subsection*{Part A}

The email protocol used is clearly SMTP, as it is the one intended to be used for sending emails between mail servers.  The other two protocols, POP and IMAP, are used for retrieving messages from mail servers.  As shown by the TCP connections created in the capture file, SMTP uses TCP as its transfer protocol.

\subsection*{Part B}

It is clear that 192.168.1.102 is the IP address of the IP address of the professor's laptop, as it is the source of the packet initializing the SMTP connection.  Since the 192.168.x.x address space is almost always used for home or small-scale Cisco routers, the laptop was likely on his home LAN.

\subsection*{Part C}

The command-line tool "dig" shown in class was used to find the IP address of mail.ecse.rpi.edu as 128.113.61.215.

\subsection*{Part D}

Using the capture file, it is not possible to directly observe the professor's password. Unlike the FTP capture file given for Problem 3, the password is not transmitted in plaintext.  It is sent using the CRAM-MD5 mechanism, which combines the users username and password with a challenge string created by the server to create a single output string hashed with the MD5 algorithm.  Deriving the plaintext password would likely require a brute force attack on the result hash.

\subsection*{Part E}

Opening the TCP stream used for transmitting the data over SMTP showed the contents of the file "testfile.txt" to be:

\begin{displayquote}
    I am testing out "blat", a Windows equivalent of the Unix command-line mailer, "mail".
\end{displayquote}

Judging by the data shown above and in the capture file, the length of the file appears to be 87 characters.

\subsection*{Part F}

The message that contains the contents of "testfile.txt" has a packet number of 36 in the capture file. However, this message also contains other information besides just the data from the file.  As defined by specifications, an email message (which is what this packet is) has a header portion before the main body.  The header is made up of multiple properties, each separated by a new line (the CRLF sequence).  Some properties are required, such as "From:" and "To:", while other like "Subject:" or "Content-Type:" are optional.  Immediately following all of the headers is a blank line, and then the message body.  In the case of this message, the contents of "testfile.txt" appear as the body. Once the body is complete, the sequence "CRLF.CRLF" is written to indicate to the server that the message is finished.

\end{document}
