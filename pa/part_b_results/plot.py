import matplotlib.pyplot as plt
import csv

def read_data(path):
    with open(path, 'rb') as f:
        reader = csv.reader(f)
        res = map(list, zip(*list(reader)[1:]))
        return res[0], res[1]

def plot_data(x, y, xlabel):
    plt.plot(x, y, marker='o')

    plt.xlabel(xlabel)
    plt.ylabel('Delivery Time (seconds)')
    plt.title('Effect of {} on Delivery Time'.format(xlabel))

    plt.show()

def read_and_plot(path, label):
    x, y = read_data(path)
    plot_data(x, y, label)

if __name__ == '__main__':
    read_and_plot('loss_rate.csv', 'Loss Rate')
    read_and_plot('average_delay.csv', 'Average Delay (milliseconds)')
