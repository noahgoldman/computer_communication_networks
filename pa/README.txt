Programming Assignment 1
Noah Goldman

Usage:

All of the code for the assignment (both parts A and B) is contained in the Python source file "ping_client.py".  This file can be invoked using command line arguments to run either part A or B.

To run part A, use "a" as the last argument to the program:

python ping_client.py <host> <port> a

To run part B, use "b" as the last argument to the program:

python ping_client.py <host> <port> b

Testing:

The functionality of both parts A and B was verified by modifying the LOSS_RATE and AVERAGE_DELAY parameters in PingServer.java and observing the resulting delivery times reported by "ping_client.py".  There was clearly a directly proportional relationship between both of the parameters and the resulting delivery times, and this is further explained in the document "plots.pdf".  This form of testing combined with logical predictions that increasing both the loss rate and average delay should increase delivery time shows that the prrogram was likely functioning correctly.

The program was also tested by running the server on a different host in the same LAN, which worked correctly.

Bugs:

There are no known bugs.
